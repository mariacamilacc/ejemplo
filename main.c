/*
 * File:   main.c
 * Author: Camila
 * Created on 16 de mayo de 2019, 01:09 AM
 * Modified on 1 de Julio de 2019
 */


#include <xc.h>
#include "configuration.h"
#define _XTAL_FREQ 32000000

void main(void) {
    
     //define variables adc, voltaje, temperatura
    int adc; //16-bit variable where value is stored from 0 to 1024 of the adc
    float volt,temp,val; //definition of floating variables of voltage and temperature
    unsigned char buffer1[16]; //array definition
    
     // Items configuration
    //Oscilator
    OSCCONbits.IRCF=0b1110; //8MHz Signal  - bits ircf configuration bits
    OSCCONbits.SCS=0b00; //config1 INTOCS
    
    //Ports configuration 
    ANSELA=0b00100000; //Definition of RA5 as analog
    TRISA=0b00100000;// RA5 as input  ( 0 output, 1 input) 
    
    //ADC CONFIGURATION
    ADCON0bits.CHS=4; //Reading analog channel 4 (channel selection with CHS)
    //Selection of reference voltages
    ADCON1bits.ADNREF=0; //Negative reference = Vss
    ADCON1bits.ADPREF=0; //positive reference = Vdd
    ADCON1bits.ADCS=0b111;//Conversion frequency
    ////Selection of result format, the result I justify it to the righ
    ADCON1bits.ADFM=1; //Justification to the right
    ADCON0bits.ADON=1; //ADC ON
    
    
    //Puerto
    TRISC=0b10000000;
    PORTC=0;
    
    //Configuration Serial Port
    TXSTAbits.SYNC=0;//Tx asynchronous
    TXSTAbits.TXEN=1;//Enable Tx
    RCSTAbits.SPEN=1;//Enable Serial Port
   
    // Baud Rate Configuration 
    BAUDCONbits.BRG16=0; //Timer generator of 8 bits
    TXSTAbits.BRGH=0;//Low Velocity
    SPBRG=51; //Chatge register Obtain from Datasheet **Baud Rate 9600
    
    while(1){
        ADCON0bits.GO_nDONE=1;
        while(ADCON0bits.GO_nDONE);
        adc=ADRESH;
        //shift to the right
        adc=adc<<8;
        adc=adc+ADRESL;
        volt=adc*5.0/1024.0; 
	val=0;
        temp=volt*100;
        unsigned short temint = (unsigned short)temp;
        unsigned short digs[2]; //
        digs[0] = 48 + temint/10; //TENS of temperture data
        digs[1] = 48 + temint - (digs[0]-48)*10;//UNITS of temperture data
            TXREG = 'T';
            while(TXSTAbits.TRMT==0);//wait for a void bufer
            
            TXREG = '=';
            while(TXSTAbits.TRMT==0);//wait for a void bufer
            
            TXREG = digs[0];//Write Tens temp data
            while(TXSTAbits.TRMT==0); //wait for a void bufer
            
            TXREG = digs[1]; //Write Units temp data
            while(TXSTAbits.TRMT==0);//wait for a void bufer
            
            TXREG = ' ';
            while(TXSTAbits.TRMT==0);//wait for a void bufer
            
            
         __delay_ms(300);
    
       
    }
    return;
}
